#!/bin/bash

while true
do
    maintenant=$(date "+%H:%M:%S - ")
    echo -n $maintenant >> /var/log/surveillemem.log
    grep Dirty /proc/meminfo >> /var/log/surveillemem.log
    sleep 30
done