#!/bin/bash

case $1 in 
    start)
        /opt/scripts/ch3_tp1_runlevels_surveillemem.sh & ;;
    stop)
        pkill ch3_tp1_runlevels_surveillemem.sh ;;
esac