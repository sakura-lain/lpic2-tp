#!/bin/bash
# Usage : ./ch13_tp2_charge.sh arg1 arg2

serv=$1 # Adresse du serveur web
iter=$2 # Nombre d'itérations
cpt=0

while [ $cpt -lt $iter ]
do 
    for i in {1..5}
    do
        wget -prq $serv/p$i.html &
    done
    cpt=$(( cpt + 1 ))
done

echo lancement fait
wait
echo fin de $cpt