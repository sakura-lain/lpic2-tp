# Préparation à la certification LPIC-2 - Travaux pratiques

Ce projet a pour but de tester et d'actualiser la réalisation des travaux pratiques décrits dans le livre de Sébastien Bobillier et Philippe Banquet *Préparation à la certification LPIC-2*, 3è édition (Editions Eni, novembre 2014). En effet le livre est un peu daté et certaines adaptations sont nécessaires à la réalisation de certains exercices.

Voir aussi : [Fiches de révision pour l'examen 201](https://gitlab.com/sakura-lain/lpic2-fiches-revision)

## Machines virtuelles

- serveur alpha : Debian 10, installation minimale
- serveur beta : CentOS 7, installation d'un serveur avec GUI comprenant un serveur DNS (Bind), un serveur de fichiers et de stockage (NFS etc.), les outils de développement
- station de travail : Ubuntu 18.04 LTS

Toutes les machines virtuelles disposent d'un accès par pont. Les adresses IP statiques sont attribuées au niveau du routeur. Les additions invités sont installées. Pour être à l'aise et parce que ma machine hôte est suffisamment puissante pour le permettre, j'ai poussé la ram des machines beta et station à 8 GB et leur ai attribué un processeur à quatre coeurs.

## OBJECTIFS 201-450

## Chapitre 12, Compilation des applications et du noyau Linux

### TP 1 : Compilation d'une application

- **serveur beta**

On compile la version 1.8.6 du client `rdesktop` sur le serveur beta. Dans cette version, il n'y a pas de fichier `configure`. Celui-ci doit être généré par la commande `autoconf`. L'installation d'Openssl est nécessaire, ainsi que l'installation de `libgssglue` et de PCSC, ou bien la désactivation des services les nécessitant (on choisit ici la deuxième option) :

    $ tar zxvf rdesktop-1.8.6.tar.gz
    $ cd rdesktop-1.8.6
    $ autoconf
    $ sudo yum install openssl-devel
    $ sudo ./configure --disable-credssp --disable-smartcard

L'installation des binaires ne nécessite aucune remarque particulière :

    $ make
    $ sudo make install
    
De même pour le nettoyage des sources :

    $ make clean
    
La commande renvoie ce qu'elle est en train de faire :

    rm -f *.o *~ vnc/*~ rdesktop rdp2vnc
    
### TP 2 : Compilation et installation d'un module de noyau

- **serveur beta**

Le module `tg3` fourni est trop ancien pour le noyau utilisé par CentOS7 (3.10). J'ai essayé en modifiant les Cflags mais sans succès : 

    make -C /lib/modules/$(uname -r)/build M=$PWD tg3
    
Une explication [ici](https://example35057.wordpress.com/2016/10/03/migration-of-linux-device-drivers-from-old-2-6-kernels-to-latest3-18-above-kernels/) ou [là](https://stackoverflow.com/questions/22662906/linux-kernel-compile-error-elf-x86-64-missing), mais ce qu'il faut faire pour résoudre le problème (modifier le Makefile) va au-delà de ce qui est requis pour la certif.

Le TP ne fonctionne pas non plus avec une CentOS 6, kernel 2.6.32-754.el6.x86_64. Cela peut venir d'une incompatibilité entre le module fourni et l'architecture pour VirtualBox 6 (voir [ici](https://www.virtualbox.org/ticket/15411) ou [là](https://sourceforge.net/p/e1000/mailman/message/35110137/)).

### TP 3 : Patcher une application

- **station**

Bien qu'on ne voie pas bien l'intérêt de copier le patch dans l'arborescence du répertoire `courses`, ce TP fonctionne sans surprise très bien sous Ubuntu 18.04.

### TP 4 : Compilation et installation d'un nouveau noyau

- **serveur alpha**

Pour la compilation du noyau 5.2.14 sur le serveur alpha, il est nécessaire d'installer au préalable les paquets `flex`, `bison`, `bc`, `libelf-dev` et `libssl-dev`. La compilation se fait avec l'utilisateur toto.

    # apt install flex bison bc libelf-dev libssl-dev
    $ wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.2.14.tar.xz
    $ tar -xJf linux.5.2.14.tar.xz
    $ cd linux.5.2.14
    $ make defconfig # génère le .config
    $ make
    
Go dodo. Zzzzz...

Pour une architecture amd64, le fichier `bzImage` se trouve dans `arch/x86_64/boot` :

    # cp arch/x86_64/boot/bzImage /boot/vmlinuz-5.2.14-amd64
    # cp .config /boot/config-5.2.14
    # make modules_install
    # ls /lib/modules
    4.19.0-6-amd64 5.2.14
    # su -
    # mkinitramfs -o initrd-5.2.14.img
    # file initrd-5.2.14.img
    initrd-5.2.14.img: gzig compressed data [...]
    # update-grub
    # reboot
    
Le système boote maintenant sur le nouveau noyau. L'ancien noyau est toujours disponible via les options avancées de grub 2.

## Chapitre 13, gestion et planification des ressources

### TP 1 : Surveillance des ressources d'un serveur

- **serveur alpha**

Ce TP ne présente pas de difficulté particulière. A ce stade on ne paut pas encore faire la courte partie sur LVM. Pour faire le reste, il suffit d'installer un serveur Apache 2, ainsi que les paquets `sysstat` (pour `iostat`) et `net-tools` (pour `netstat`). La commande *ifconfig*, dépréciée, peut être remplacée par *ip a* et il est à noter que le dernier exercice (afficher une connexion entrante sur le serveur web) ne fonctionne que si la connexion entrante provient d'une autre machine virtuelle, par exemple station.

### TP 2 : Planification de charge

- **serveurs alpha et beta**

Sur le serveur alpha, on va copier les fichiers du chapitre 13 dans /var/www/html. En se situant dans le répertoire d'origine de ces fichiers :

    # cp /var/www/html/index.html /var/www/html/index.html.bak # Sauvegarde du fichier d'index original d'Apache
    # cp -r . /var/www/html/
    # cd /var/www/html/

Création des pages web additionnelles :

    # for i in {1..10}; do cp index.html p${i}.html; done

Le script indiqué dans le livre contient quelques erreurs et peut être amélioré. Utiliser le script `ch13_tp2_charge_testapache.sh` joint à la place, en indiquant comme arguments le nom ou l'adresse du serveur alpha et le nombre d'itérations souhaitées. A noter que dans le cas d'un système puissant, il peut être nécessaire de monter jusqu'à 10 000 pour constater quelque chose de notable (y compris des messages d'erreur sur beta indiquant qu'une limite de ressources a été atteinte) :

    # ./ch13_tp2_charge_testapache.sh alpha 10000

OpenWebLoad est trop ancien pour être correctement compilé, même après avoir converti le fichier `configure` au format unix avec la commande `dos2unix`.

## Chapitre 2, gestion du stockage

- **Serveur alpha**

### TP1 : Exploitation d'un espace de swap sur fichier

Pas de difficultés particulières.

### TP2 : Configuration d'un disque en raid 0

TP à réaliser avec le noyau 4.19.0-6-amd64, et non avec le dernier noyau précédemment compilé. A partir de ce stade et tant que RAID est ativé, c'est ce noyau qu'il faudra utiliser.

### TP3 : Création et exploitation d'un volume logique sur le disque RAID0

Pas de remarque particulière.

### TP 4 : gestion d'un filesystem XFS

On ajoute un périphérique USB virtuel à notre machine. A ce stade de nos expérimentations elle est reconnue en tant que périphérique en mode bloc `/dev/sdd`

La commande `xfs_info` prend comme argument le point de montage et non le périphérique en mode bloc :

    xfs_info /var/cloud
    
La commande `xfs_check` est obsolète. Utiliser à la place `xfs_repair` avec l'option `-n` :

    xfs_repair -n /dev/sdd
    
La commande `xfs_growfs` prend comme argument le point de montage :

    cd /var/cloud
    xfs_growfs -d .
    
La taille de la clé est passée de 92 Mo à 147 Mo et son taux d'occupation de 17% à 12 %.

## Chapitre 3, démarrage du système

### TP 1 : Création d'un niveau d'exécution sur mesure avec applications spécifiques

Utiliser les scripts `ch3_tp1_runlevels_surveillemem.sh` et  `ch3_tp1_runlevels_surveillemem_init.sh`.

`pgrep` ne renvoie curieusement aucun résultat. Pour contrôler l'activité du processus :

    ps -ef | grep surveillemem
    
Le TP ne peut pas être poursuivi sous Debian 10 (`update-rc.d` ne crée pas de liens symboliques) ni sous CentOS 7 (pour qui `chkconfig` est déprécié)

### TP 2 : Réinstallation de Grub 1 après corruption

Sur la version la plus récente de DSL, grub se lance avec des erreurs puis ne reconnaît ni le disque ni aucune commande. Peut-être une incompatibilité avec VirtualBox 6 ?



